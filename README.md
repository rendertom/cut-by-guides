# Cut by Guides #
Script for Adobe Photoshop to cut image into small peaces defined by Guides.

Script saves and names files according to their grid number/letter. Rows are named with numbers [0-9] and columns by letters [A-Z]. Each slice is saved as PNG file with original name + grid number&letter.

Have one layer in the file. Add bunch of Guides. Run the script. Enjoy the rest of your day!

### Installation ###
Clone or download this repository and place **Cut by Guides.jsx** script to Photoshop’s Scripts folder:

```Adobe Photoshop CC 20XX -> Presets -> Scripts -> Cut by Guides.jsx```

Restart Photoshop to access **Cut by Guides** script from File -> Scripts

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------